# RCPlayer #
![Release](https://img.shields.io/badge/release-v2.5.7-green.svg)

The aim of this project is to provide a easy way to use the player of Radio-Canada.

## Features

* Easy to use
* Super lightweight
* 16x9 auto size
* Custom poster + button play
* Control all players in the same time
* More methods (Global and local)
* Better way integration (JS or Attr)
* Easy Events Callback
* Debug Mode
* Custom Name
* Mods (Absolute, FancyBox, FIXED)
* Add Global String events on Player by Attr
* All events return the RCPlayer
* Compatible accessibility

## TO DO

# Dependencies

* jQuery
* FancyBox 3.2.5 (*optional - only for FancyBox mod*)
* [Console Audio-Video RC](https://docs.google.com/document/d/1KUfE9DU4o6Wa4pH_QMVhYb1OPxJLQJmursEyKUfB3OU/edit)

# Installing

* `bower install https://bitbucket.org/version10/v10core-rcplayer.git --save`
* `<script type="text/javascript" src="https://services.radio-canada.ca/media/player/client/{idClient}"></script>`

## Using

```javascript
    var rc = new V10Core.RCPlayer({OPTIONS});
```

## Options.core

Core | Type | Default | Description
------------ | ------ | --------- | -------------
container | String | `null` | container name selector
name | String | `''` | Custom name (optional)
debugMode | Boolean | `false` | Active Debug Mode (Logs Help)

## Options.media

Media | Type | Default | Description
------------ | ------ | --------- | -------------
idMedia | String | `'7561489'` | id media
appCode | String | `'medianet'` | app code

## Options.ui

Media | Type | Default | Description
------------ | ------ | --------- | -------------
poster | String | `''` | poster image
btnPlay | String | `'<i class='fa fa-play-circle-o fa-5x' aria-hidden='true'></i>'` | btn play

## Options.rcParams

RC Params | Type | Default | Description
------------ | ------ | --------- | -------------
autoPlay | Boolean | `false` | Auto play the player on init
width | String | `'100%'` | width of the player
height | String | `'100%'` | height of the player
urlTeaser | String | `null` | Poster image
canExtract | Boolean | `false` | Can extract the video
time | Number | `0` | Start time to seek
infoBar | Boolean | `false` | Show the info bar
share | Boolean | `false` | Can share the video
multiInstance | Boolean | `false` | Can play multiple video in the same time
lang | String | `'fr'` | Default language

## Options.events

Events | Type | Default | Description
------------ | ------ | --------- | ----------------
PLAYER_UI_READY | Function | `null` | Call After the UI ready (poster + buttons)
PLAYER_RC_READY | Function | `null` | Call After the player is ready and events ready
BEGIN | Function | `null` | *radio-canada event*
END | Function | `null` | *radio-canada event*
START | Function | `null` | *radio-canada event*
FIRST_PLAY | Function | `null` | Call After the first play
PLAY | Function | `null` | *radio-canada event*
PAUSE | Function | `null` | *radio-canada event*
EXTRACT | Function | `null` | *radio-canada event*
SEEKED | Function | `null` | *radio-canada event*
CURRENT_TIME_CHANGE | Function | `null` | *radio-canada event*
PRESENCE | Function | `null` | *radio-canada event*
AD_STARTED | Function | `null` | *radio-canada event*
AD_PAUSE | Function | `null` | *radio-canada event*
AD_COMPLETE | Function | `null` | *radio-canada event*
NEXT | Function | `null` | *radio-canada event*
PREVIOUS | Function | `null` | *radio-canada event*
SHARE | Function | `null` | *radio-canada event*
VOLUME_CHANGE | Function | `null` | *radio-canada event*
MUTE | Function | `null` | *radio-canada event*
UNMUTE | Function | `null` | *radio-canada event*
ENTER_FULL_SCREEN | Function | `null` | *radio-canada event*
EXIT_FULL_SCREEN | Function | `null` | *radio-canada event*
COMPLETE | Function | `null` | *radio-canada event*
ERROR | Function | `null` | *radio-canada event*
META_LOADED | Function | `null` | *radio-canada event*
NEXT_CHAPTER | Function | `null` | *radio-canada event*
UNMUTE | Function | `null` | *radio-canada event*
END_CHAPTER | Function | `null` | *radio-canada event*

## Options.mod

Mod | Description
------------ | ---------
DEFAULT | NO Mod (Default player)
ABSOLUTE | Show the player on top while playing
FANCY_BOX | Show the player on FancyBox3
FIXED | Show the player on top and fallow

## Example RC Player (Default + PLAY event)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.RCPlayer({
            core:{
                container:"[data-player-container]"
            },
            media:{
                 idMedia:"7561489"
            },
            events: {
                PLAY: function (rcPlayer) {
                    console.log("play!");
                }
            }
        });

    });
```

## Example RC Player (Custom UI)

```javascript
   jQuery(document).ready(function($) {

       var rc = new V10Core.RCPlayer({
           core:{
               container:"[data-player-container]"
           },
           media:{
                idMedia:"7561489"
           },
           ui: {
               poster: "http://via.placeholder.com/990x556"
           }
       });

   });
```

## Example RC Player (Custom UI + Fancybox)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.RCPlayer({
            core:{
                container:"[data-player-container]"
            },
            media:{
                 idMedia:"7561489"
            },
            rcParams:{
                autoPlay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            },
            mod:V10Core.RCPlayer.mods.FANCY_BOX
        });

    });
```

## Example RC Player (Open Fancybox on page load)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.RCPlayer({
            media:{
                 idMedia:"7561489"
            },
            rcParams:{
                autoPlay:true
            },
            mod:V10Core.RCPlayer.mods.FANCY_BOX
        });

    });
```

## Example RC Player (Absolute)

```javascript
       jQuery(document).ready(function($) {

           var rc = new V10Core.RCPlayer({
               core:{
                   container:"[data-player-container]"
               },
               media:{
                    idMedia:"7561489"
               },
               rcParams:{
                   autoPlay:true
               },
               ui: {
                   poster: "http://via.placeholder.com/990x556"
               },
               mod:V10Core.RCPlayer.mods.ABSOLUTE
           });

       });
```

## Example RC Player (Fixed)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.RCPlayer({
            core:{
                container:"[data-player-container]"
            },
            media:{
                 idMedia:"7561489"
            },
            rcParams:{
                autoPlay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            },
            mod:V10Core.RCPlayer.mods.FIXED
        });

    });
```

```CSS
    .v10core-rcplayer-mod-fixed{
        max-width: 25vw;
        max-height: 25vh;
        top:15px;
        left:15px;
    }
```

## Example RC Player (HTML Attr method)

```HTML
    <div data-v10core-rcplayer='{"media": {"idMedia":"7561489"}}'></div>
```

## Example RC Player (HTML Custom Attr method + Ajax)

```HTML
    <div data-custom-attr='{"media": {"idMedia":"7561489"}}'></div>
```

```javascript
    jQuery(document).ready(function($) {
        V10Core.RCPlayer.initPlayerByAttr("data-custom-attr");
    });
```

## Example RC Player (HTML Attr method + Ajax)

```HTML
    <div data-v10core-rcplayer='{"media": {"idMedia":"7561489"}, "events": {"PLAY":"onPlayVideo"}}'></div>
```

```javascript
    jQuery(document).ready(function($) {
        V10Core.RCPlayer.initPlayerByAttr(V10Core.RCPlayer.rcPlayerAttribute);
    });

    function onPlayVideo(rcPlayer){

    }
```


## Global Methods

*Pause*

Pause all videos

```
    V10Core.RCPlayer.pause();
```

*Stop*

Stop all videos

```
    V10Core.RCPlayer.stop();
```

*players*

Return the list of RCPlayer

```
    V10Core.RCPlayer.players
```

*count*

Return the number of RCPlayer

```
    V10Core.RCPlayer.count
```

*initPlayerByAttr*

Create new players by the attribute

```
    V10Core.RCPlayer.initPlayerByAttr(V10Core.RCPlayer.rcPlayerAttribute);

    V10Core.RCPlayer.initPlayerByAttr("data-custom-attr");
```



## Local Methods

*Play*

Play the video

```
    rc.play();
```

*Pause*

Pause the video

```
    rc.pause();
```

*Stop*

Stop the video

```
    rc.stop();
```

*Change*

Change the video idMedia (idMedia, CUSTOM_RC_PARAMS)

```
    rc.change("7561489", {autoPlay:true});
```

### Credits ###

- Michaël Chartrand (SirKnightDragoon)