//Class Name
var className = "RCPlayer";

//Import
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var pump = require('pump');

//Script
gulp.task('scripts', function(cb){
    pump([
        gulp.src('./src/js/**/*.js'),
        concat(className+'.min.js'),
        uglify({output:{comments : true}}),
        gulp.dest('./dist/')
    ],cb)
});

// Default Task
gulp.task('default', ['scripts']);