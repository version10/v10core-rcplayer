//RCPlayer v2.5.7
if(typeof V10Core === "undefined") var V10Core = {};

V10Core.RCPlayer = function(options){

    //Init Options
    this.options = {};

    //RCPlayer Core options
    this.options.core = {
        name:"",
        container:null,
        debugMode:false
    };

    //Media options
    this.options.media = {
        idMedia:"7561489",
        appCode:"medianet"
    }

    //Custom UI option
    this.options.ui = {
        poster:"",
        btnPlay:"<i class='fa fa-play-circle-o fa-5x' aria-hidden='true'></i>"
    }

    //Radio-canada rcPlayer options
    this.options.rcParams = {
        autoPlay: false,
        width: "100%",
        height: "100%",
        urlTeaser:null,
        canExtract:false,
        time:0,
        infoBar:false,
        share:false,
        multiInstance:false,
        lang: "fr"
    };

    //Events options callback
    this.options.events = {
        BEGIN:null,
        END:null,
        FIRST_PLAY:null,
        START:null,
        PLAY:null,
        PAUSE:null,
        EXTRACT:null,
        SEEKED :null,
        CURRENT_TIME_CHANGE:null,
        PRESENCE:null,
        AD_STARTED:null,
        AD_PAUSE:null,
        AD_COMPLETE:null,
        NEXT:null,
        PREVIOUS:null,
        SHARE:null,
        VOLUME_CHANGE:null,
        MUTE:null,
        UNMUTE:null,
        ENTER_FULL_SCREEN:null,
        EXIT_FULL_SCREEN:null,
        COMPLETE:null,
        ERROR:null,
        META_LOADED:null,
        NEXT_CHAPTER:null,
        END_CHAPTER:null
    }

    //Mod options
    this.options.mod = V10Core.RCPlayer.mods.DEFAULT;

    //Add all user options in the options group
    for (key in options.core) {
        this.options.core[key] = options.core[key];
    }

    for (key in options.media) {
        this.options.media[key] = options.media[key];
    }

    for (key in options.ui) {
        this.options.ui[key] = options.ui[key];
    }

    for (key in options.rcParams) {
        this.options.rcParams[key] = options.rcParams[key];
    }

    for (key in options.events) {
        this.options.events[key] = options.events[key];

        if(typeof options.events[key] == "string"){
            this.options.events[key] = window[options.events[key]];
        }
    }

    //Mod options
    if(options.mod) this.options.mod = options.mod;

    //Add the player in the list
    V10Core.RCPlayer.players.push(this);
    V10Core.RCPlayer.count++;

    //Create UID of the player
    this.uid = "v10core-rcplayer-" + V10Core.RCPlayer.count;

    //Set default name
    if(this.options.core.name == "") this.options.core.name = this.uid;

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: created");

    //Init player ui
    this.initUI();

    //Properties
    this.firstPlay = false;
    this.isAds = false;

    //Init the player by mod selected
    if(this.options.mod == V10Core.RCPlayer.mods.DEFAULT && this.options.ui.poster == ""){
        this.initDefaultPlayer();
    }
    if(this.options.mod == V10Core.RCPlayer.mods.FANCY_BOX && this.options.ui.poster == ""){
        this.initFancyboxPlayer();
    }
}

//Function initUI
// -----------------------------------------------------
// Init the UI of the player
V10Core.RCPlayer.prototype.initUI = function(){
    //Insert the uid in the core container
    if(this.options.core.container != null){

        //CSS 16x9 Box
        $(this.options.core.container).css({
            position: "relative",
            display: "block",
            height: 0,
            padding: 0,
            paddingBottom: "56.25%"
        });

        //Init core ui group
        this.ui = {};

        //Create the player container ui
        this.ui.player = $("<div class='v10core-rcplayer-ui-player' "+this.uid+"-player></div>");
        this.ui.player.attr("id", this.uid);

        $(this.ui.player).css({
            position: "relative",
            display: "block",
            width: "100%",
            height: "100%"
        });

        $(this.options.core.container).append(this.ui.player);

        //Add poster image
        if(this.options.ui.poster != ""){

            this.ui.poster = $("<a href='#' class='v10core-rcplayer-ui-poster' "+this.uid+"-ui-poster><span style='display: none;'>Écouter la vidéo</span></a>");

            this.ui.poster.css({
                position: "absolute",
                top:0,
                left:0,
                display: "block",
                width: "100%",
                height: "100%",
                backgroundImage: "url('"+this.options.ui.poster+"')",
                backgroundRepeat:"no-repeat",
                backgroundSize:"cover",
                zIndex:2,
                cursor:"pointer"
            });

            $(this.options.core.container).append(this.ui.poster);

            if(this.options.ui.btnPlay != ""){
                this.ui.btnPlay = $(this.options.ui.btnPlay);
                this.ui.btnPlay.attr(this.uid+"-ui-btn-play", "");
                this.ui.btnPlay.addClass("v10core-rcplayer-ui-btn-play");

                this.ui.btnPlay.css({
                    position:"absolute",
                    top: "50%",
                    left: "50%",
                    webkitTransform: "translate(-50%,-50%)",
                    transform: "translate(-50%,-50%)",
                    zIndex:3,
                    pointerEvents: "none"
                });

                $(this.options.core.container).append(this.ui.btnPlay);
            }

            this.ui.poster.on("click", this.onUIEventPlay.bind(this));
        }
    }

    if(this.options.events.PLAYER_UI_READY != null) this.options.events.PLAYER_UI_READY();
}

//Function initDefaultPlayer
// -----------------------------------------------------
// Init the default player
V10Core.RCPlayer.prototype.initDefaultPlayer = function(){
    this.player = new RadioCanada.player(this.uid, {
        appCode: this.options.media.appCode,
        idMedia: this.options.media.idMedia,
        params: this.options.rcParams
    });

    this.player.addEventListener(RadioCanada.player.events.READY, this.eventReady.bind(this));

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: initDefaultPlayer");
}

//Function initFancyboxPlayer
// -----------------------------------------------------
// Init the fancybox player
V10Core.RCPlayer.prototype.initFancyboxPlayer = function(){
    var _this = this;

    var fancyboxHTML = $("<div></div>");
    var fancyboxWindow = $("<div style='width: 100%; height:100%; overflow: hidden; padding:0;'></div>");
    var fancyboxPlayerCnt = $("<div></div>");

    fancyboxPlayerCnt.attr("id", this.uid+'-fancybox');

    fancyboxWindow.append(fancyboxPlayerCnt);
    fancyboxHTML.append(fancyboxWindow);

    $.fancybox.open(fancyboxHTML.html(), {
        infobar: true,
        buttons : [
            'close'
        ],
        smallBtn : false,
        touch : false,
        onUpdate : function( instance, current ) {
            var width,
                height,
                ratio = 16 / 9,
                video = current.$content;

            if ( video ) {
                width  = current.$slide.width();
                height = current.$slide.height() - 100;

                if ( height * ratio > width ) {
                    height = width / ratio;
                } else {
                    width = height * ratio;
                }

                video.css({
                    width  : width,
                    height : height
                })
            }
        },
        afterLoad:function(instance, current){

            var video = current.$content;

            video.css({
                background  : "none"
            });

            _this.player = new RadioCanada.player(_this.uid+"-fancybox", {
                appCode: _this.options.media.appCode,
                idMedia: _this.options.media.idMedia,
                params: _this.options.rcParams
            });

            _this.player.addEventListener(RadioCanada.player.events.READY, _this.eventReady.bind(_this));
        }
    });

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: initFancyboxPlayer");
}

//Function initAbsolutePlayer
// -----------------------------------------------------
// Init the absolute player
V10Core.RCPlayer.prototype.initAbsolutePlayer = function(){
    var _this = this;

    var absolutePlayerCnt = $("<div></div>");
    absolutePlayerCnt.attr("id", this.uid+'-absolute');
    absolutePlayerCnt.addClass('v10core-rcplayer-mod-absolute');
    absolutePlayerCnt.css({
        width:$(this.options.core.container).innerWidth(),
        height:$(this.options.core.container).innerHeight(),
        position:"absolute",
        zIndex:"9999999999",
        top:$(this.options.core.container).offset().top,
        left:$(this.options.core.container).offset().left,
        background:"#000"
    });
    $("body").append(absolutePlayerCnt);

    $(window).on("resize", function(){
        absolutePlayerCnt.css({
            width:$(_this.options.core.container).innerWidth(),
            height:$(_this.options.core.container).innerHeight(),
            top:$(_this.options.core.container).offset().top,
            left:$(_this.options.core.container).offset().left
        });
    });

    this.player = new RadioCanada.player(this.uid+'-absolute', {
        appCode: this.options.media.appCode,
        idMedia: this.options.media.idMedia,
        params: this.options.rcParams
    });

    this.player.addEventListener(RadioCanada.player.events.READY, this.eventReady.bind(this));

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: initAbsolutePlayer");
}

//Function initFixedPlayer
// -----------------------------------------------------
// Init the fixed player
V10Core.RCPlayer.prototype.initFixedPlayer = function(){
    var _this = this;

    var fixedPlayerCnt = $("<div></div>");
    fixedPlayerCnt.attr("id", this.uid+'-fixed');
    fixedPlayerCnt.addClass('v10core-rcplayer-mod-fixed');
    fixedPlayerCnt.css({
        position:"fixed",
        zIndex:"9999999999"
    });
    $("body").append(fixedPlayerCnt);

    function updateFixedSize(){
        var ratio = 16 / 9;

        var width  = parseInt(fixedPlayerCnt.css("maxWidth"));
        var height = parseInt(fixedPlayerCnt.css("maxHeight"));

        if ( height * ratio > width ) {
            height = width / ratio;
        } else {
            width = height * ratio;
        }

        fixedPlayerCnt.css({
            width  : width,
            height : height
        })
    }

    $(window).on("resize", updateFixedSize);

    updateFixedSize();

    this.player = new RadioCanada.player(this.uid+'-fixed', {
        appCode: this.options.media.appCode,
        idMedia: this.options.media.idMedia,
        params: this.options.rcParams
    });

    this.player.addEventListener(RadioCanada.player.events.READY, this.eventReady.bind(this));

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: initAbsolutePlayer");
}

//Method play
// -----------------------------------------------------
// play the player
V10Core.RCPlayer.prototype.play = function(){
    if(!this.firstPlay) return;
    this.player.play();

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: play");
}

//Method pause
// -----------------------------------------------------
// pause the player
V10Core.RCPlayer.prototype.pause = function(){
    if(!this.firstPlay) return;
    this.player.pause();

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: pause");
}

//Method stop
// -----------------------------------------------------
// stop the player
V10Core.RCPlayer.prototype.stop = function(){
    if(!this.firstPlay) return;
    this.player.pause();
    this.seek(0);

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: stop");
}

//Method seek
// -----------------------------------------------------
// seek the player
V10Core.RCPlayer.prototype.seek = function(pos){
    if(!this.firstPlay || this.isAds) return;
    this.player.seek(pos);

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: seek", pos);
}

//Method change
// -----------------------------------------------------
// change the media in the player
V10Core.RCPlayer.prototype.change = function(idMedia, rcParams){
    for (key in rcParams) {
        this.options.rcParams[key] = rcParams[key];
    }

    this.options.media.idMedia = idMedia;

    this.player.change({
        appCode: this.options.media.appCode,
        idMedia: this.options.media.idMedia,
        params: this.options.rcParams
    });

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: change", idMedia, rcParams);
}

//Function eventReady
// -----------------------------------------------------
// Init all the events of the player
V10Core.RCPlayer.prototype.eventReady = function(){
    this.player.addEventListener(RadioCanada.player.events.BEGIN, this.onBeginCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.END, this.onEndCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.START, this.onStartCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.PLAY, this.onPlayCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.PAUSE, this.onPauseCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.EXTRACT, this.onExtractCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.SEEKED || RadioCanada.player.events.SEEK, this.onSeekedCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.CURRENT_TIME_CHANGE, this.onCurrentTimeChangeCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.PRESENCE, this.onPresenceCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.AD_STARTED, this.onAdStartedCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.AD_PAUSE, this.onAdPauseCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.AD_COMPLETE, this.onAdCompleteCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.NEXT, this.onNextCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.PREVIOUS, this.onPreviousCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.SHARE, this.onShareCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.VOLUME_CHANGE, this.onVolumeChangeCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.MUTE, this.onMuteCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.UNMUTE, this.onUnMuteCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.ENTER_FULL_SCREEN, this.onEnterFullScreenCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.EXIT_FULL_SCREEN, this.onExitFullScreenCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.COMPLETE, this.onCompleteCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.ERROR, this.onErrorCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.META_LOADED, this.onMetaLoadedCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.NEXT_CHAPTER, this.onNextChapterCallback.bind(this));
    this.player.addEventListener(RadioCanada.player.events.END_CHAPTER, this.onEndChapterCallback.bind(this));

    if(this.options.events.PLAYER_RC_READY != null) this.options.events.PLAYER_RC_READY();

    this.bugFix();

    this.debugLog("RCPlayer :: " + this.options.core.name + " :: eventReady");
}

//Event onBeginCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onBeginCallback = function(){
    if(this.options.events.BEGIN != null) this.options.events.BEGIN(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: BEGIN");

    if(!this.firstPlay){
        this.firstPlay = true;

        if(this.options.events.FIRST_PLAY != null) this.options.events.FIRST_PLAY(this);
        this.debugLog("RCPlayer :: " + this.options.core.name + " :: FIRST_PLAY");
    }
}

//Event onEndCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onEndCallback = function(){
    if(this.options.events.END != null) this.options.events.END(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: END");
}

//Event onStartCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onStartCallback = function(){
    if(this.options.events.START != null) this.options.events.START(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: START");
}

//Event onPlayCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onPlayCallback = function(){
    if(this.options.events.PLAY != null) this.options.events.PLAY(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: PLAY");

    if(!this.firstPlay){
        this.firstPlay = true;

        if(this.options.events.FIRST_PLAY != null) this.options.events.FIRST_PLAY(this);
        this.debugLog("RCPlayer :: " + this.options.core.name + " :: FIRST_PLAY");
    }
}

//Event onPauseCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onPauseCallback = function(){
    if(this.options.events.PAUSE != null) this.options.events.PAUSE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: PAUSE");
}

//Event onExtractCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onExtractCallback = function(){
    if(this.options.events.EXTRACT != null) this.options.events.EXTRACT(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: EXTRACT");
}

//Event onSeekedCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onSeekedCallback = function(seek){
    if(this.options.events.SEEKED != null) this.options.events.SEEKED(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: SEEKED", seek);
}

//Event onCurrentTimeChangeCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onCurrentTimeChangeCallback = function(){
    if(this.options.events.CURRENT_TIME_CHANGE != null) this.options.events.CURRENT_TIME_CHANGE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: CURRENT_TIME_CHANGE");
}

//Event onPresenceCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onPresenceCallback = function(){
    if(this.options.events.PRESENCE != null) this.options.events.PRESENCE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: PRESENCE");
}

//Event onAdStartedCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onAdStartedCallback = function(){
    if(this.options.events.AD_STARTED != null) this.options.events.AD_STARTED(this);
    this.isAds = true;
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: AD_STARTED");

    if(!this.firstPlay){
        this.firstPlay = true;

        if(this.options.events.FIRST_PLAY != null) this.options.events.FIRST_PLAY(this);
        this.debugLog("RCPlayer :: " + this.options.core.name + " :: FIRST_PLAY");
    }
}

//Event onAdPauseCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onAdPauseCallback = function(){
    if(this.options.events.AD_PAUSE != null) this.options.events.AD_PAUSE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: AD_PAUSE");
}

//Event onAdCompleteCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onAdCompleteCallback = function(){
    if(this.options.events.AD_COMPLETE != null) this.options.events.AD_COMPLETE(this);
    this.isAds = false;
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: AD_COMPLETE");
}

//Event onNextCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onNextCallback = function(){
    if(this.options.events.NEXT != null) this.options.events.NEXT(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: NEXT");
}

//Event onPreviousCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onPreviousCallback = function(){
    if(this.options.events.PREVIOUS != null) this.options.events.PREVIOUS(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: PREVIOUS");
}

//Event onShareCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onShareCallback = function(){
    if(this.options.events.SHARE != null) this.options.events.SHARE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: SHARE");
}

//Event onVolumeChangeCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onVolumeChangeCallback = function(volume){
    if(this.options.events.VOLUME_CHANGE != null) this.options.events.VOLUME_CHANGE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: VOLUME_CHANGE", volume);
}

//Event onMuteCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onMuteCallback = function(){
    if(this.options.events.MUTE != null) this.options.events.MUTE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: MUTE");
}

//Event onUnMuteCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onUnMuteCallback = function(){
    if(this.options.events.UNMUTE != null) this.options.events.UNMUTE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: UNMUTE");
}

//Event onEnterFullScreenCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onEnterFullScreenCallback = function(){
    if(this.options.events.ENTER_FULL_SCREEN != null) this.options.events.ENTER_FULL_SCREEN(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: ENTER_FULL_SCREEN");
}

//Event onExitFullScreenCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onExitFullScreenCallback = function(){
    if(this.options.events.EXIT_FULL_SCREEN != null) this.options.events.EXIT_FULL_SCREEN(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: EXIT_FULL_SCREEN");
}

//Event onCompleteCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onCompleteCallback = function(){
    if(this.options.events.COMPLETE != null) this.options.events.COMPLETE(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: COMPLETE");
}

//Event onErrorCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onErrorCallback = function(){
    if(this.options.events.ERROR != null) this.options.events.ERROR(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: ERROR");
}


//Event onMetaLoadedCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onMetaLoadedCallback = function(metas){
    if(this.options.events.META_LOADED != null) this.options.events.META_LOADED(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: META_LOADED", metas);
}

//Event onNextChapterCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onNextChapterCallback = function(){
    if(this.options.events.NEXT_CHAPTER != null) this.options.events.NEXT_CHAPTER(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: NEXT_CHAPTER");
}

//Event onEndChapterCallback
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onEndChapterCallback = function(){
    if(this.options.events.END_CHAPTER != null) this.options.events.END_CHAPTER(this);
    this.debugLog("RCPlayer :: " + this.options.core.name + " :: END_CHAPTER");
}

//UI Event onUIEventPlay
// -----------------------------------------------------
V10Core.RCPlayer.prototype.onUIEventPlay = function(){
    //Init the player by mod selected
    if(this.options.mod == V10Core.RCPlayer.mods.DEFAULT){
        if(this.ui.poster) this.ui.poster.hide();
        if(this.ui.btnPlay) this.ui.btnPlay.hide();
        this.initDefaultPlayer();
    }
    if(this.options.mod == V10Core.RCPlayer.mods.FANCY_BOX){
        this.initFancyboxPlayer();
    }
    if(this.options.mod == V10Core.RCPlayer.mods.ABSOLUTE){
        this.initAbsolutePlayer();
    }
    if(this.options.mod == V10Core.RCPlayer.mods.FIXED){
        this.initFixedPlayer();
    }
}

//Method global pause
// -----------------------------------------------------
// pause all players
V10Core.RCPlayer.pause = function(){
    for(var i = 0; i < V10Core.RCPlayer.count; i++){
        V10Core.RCPlayer.players[i].pause();
    }
}

//Method global stop
// -----------------------------------------------------
// stop all players
V10Core.RCPlayer.stop = function(){
    for(var i = 0; i < V10Core.RCPlayer.count; i++){
        V10Core.RCPlayer.players[i].stop();
    }
}

//Event bugFix
// -----------------------------------------------------
V10Core.RCPlayer.prototype.bugFix = function(){
    $(this.options.core.container).find("button:not([type])").attr("type", "button");
}

//Function debugLog
// -----------------------------------------------------
// Debug log utils
V10Core.RCPlayer.initPlayerByAttr = function(attr){
    var attrList = $("["+attr+"]:not([v10core-rcplayer-uAttr])").toArray();

    for(var i = 0; i < attrList.length; i++){

        V10Core.RCPlayer.attrCount++;
        var uContainerID = "v10core-rcplayer-uAttr";
        $(attrList[i]).attr(uContainerID,(V10Core.RCPlayer.count+1));
        var options = JSON.parse($(attrList[i]).attr(attr));
        if(!options.core) options.core = {};
        options.core.container = "["+uContainerID+"="+(V10Core.RCPlayer.count+1)+"]";

        var rc = new V10Core.RCPlayer(options);
    }
}

//Function debugLog
// -----------------------------------------------------
// Debug log utils
V10Core.RCPlayer.prototype.debugLog = function(){
    if(this.options.core.debugMode){
        console.log(arguments);
    }
}

V10Core.RCPlayer.players = [];
V10Core.RCPlayer.count = 0;
V10Core.RCPlayer.rcPlayerAttribute = 'data-v10core-rcplayer';
V10Core.RCPlayer.mods = {DEFAULT:"default",ABSOLUTE:"absolute",FANCY_BOX:"fancybox",FIXED:"fixed"};
V10Core.RCPlayer.version = "2.5.7";

jQuery(document).ready(function(e){
    V10Core.RCPlayer.initPlayerByAttr(V10Core.RCPlayer.rcPlayerAttribute);
});