<?php
    $medianetId = (isset($_GET['id']))?$_GET['id']:'7561489';
    $clientId = (isset($_GET['client']))?$_GET['client']:'radiocanadaca_externe';

?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <!-- * Remplacer tous les @@@ de la page par une valeur appropriée * -->

    <meta charset="utf-8"/>
    <meta name="robots" content="index, follow"/>
    <script src="//assets.adobedtm.com/2eda04f28b1fa2bbcd3ec449dfdc174232ed3359/satelliteLib-74306c976638b05ba3f239cf5faa790c1a5b5fda.js"></script>

    <!-- ********** @1 ********** -->
    <!-- * Le titre de la page doit refléter le contenu de la page * -->
    <title>@@@1 | ICI Radio-Canada.ca</title>

    <meta name="viewport"
          content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1, user-scalable=no"/>

    <!-- ********** @2 ********** -->
    <meta name="dc.title" content="@@@1 | ICI Radio-Canada.ca"/>

    <meta name="Keywords" content="@@@3a"/>
    <meta name="dc.keywords" content="@@@3a"/>
    <meta name="Description" content="@@@3b"/>
    <meta name="dc.description" content="@@@3b"/>

    <!-- ********** @3 ********** -->
    <meta property="og:url" content="@@@4a"/>
    <meta property="og:site_name" content="Radio-Canada"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@@@1 | ICI Radio-Canada.ca"/>
    <meta property="og:description" content="@@@3b"/>
    <meta property="og:image" content="@@@4b"/>

    <!-- ********** @4 ********** -->
    <meta name="Copyright" content="Société Radio-Canada"/>
    <meta name="dc.publisher" content="ICI Radio-Canada"/>
    <meta name="dc.creator" content="Zone @@@4 - ICI Radio-Canada.ca"/>
    <meta name="dc.date.created" content="2015-07-17"/>
    <meta name="dc.date.modified" content="2015-07-17"/>
    <meta name="dc.language" content="fr-ca"/>

    <!-- ********** @5 ********** -->
    <!-- * métadonnées statistiques et publicités - NE mettez PAS vous-même de valeurs ("content") dans ces tags :
        mettez cette liste dans votre page sans remplir les "content", tant que vous n'avez pas reçu la liste avec les bonnes valeurs (CHAQUE page aura ses valeurs propres) * -->
    <meta id="metaWebTrends" name="WT.sp" content="global;@@@5"/>
    <meta name="WT.clic" content=""/>

    <meta name="rc.domaine" content=""/>
    <meta name="rc.application" content=""/>
    <meta name="rc.formatApplication" content=""/>
    <meta name="rc.section" content=""/>
    <meta name="rc.groupeSection" content=""/>
    <meta name="rc.segment" content=""/>
    <meta name="rc.pageSegment" content=""/>
    <meta name="rc.codePage" content=""/>
    <meta name="rc.niveau" content=""/>
    <meta name="rc.titre" content=""/>
    <meta name="rc.typeDocument" content=""/>
    <meta name="rc.typeContenu" content=""/>
    <meta name="rc.theme" content=""/>
    <meta name="rc.sousTheme" content=""/>
    <meta name="rc.dateCreation" content=""/>
    <meta name="rc.region" content=""/>
    <meta name="rc.blogue" content=""/>
    <meta name="rc.sport" content=""/>
    <meta name="rc.emission" content=""/>
    <meta name="rc.reseau" content=""/>
    <meta name="rc.chaine" content=""/>
    <meta name="rc.saison" content=""/>

    <!-- ********** @6 ********** -->
    <link href="http://ici.radio-canada.ca/content/generated/main.min.css?v=1.0.0.26060" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://ici.radio-canada.ca/Content/component.DynamicMenu.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://ici.radio-canada.ca/Content/component.StickyNav.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://ici.radio-canada.ca/Content/component.Menuzone.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://ici.radio-canada.ca/Content/component.Footer.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://s.radio-canada.ca/_css/modules/modules.barrepartagee.1.0.0.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="http://s.radio-canada.ca/mp/modules.socialbar.1.0.0.css"/>

    <script>
        window.appHost = 'http://ici.radio-canada.ca';
    </script>

    <script src='http://ici.radio-canada.ca/Content/generated/main.min.js'></script>
    <script src='http://ici.radio-canada.ca/Content/generated/app.min.js'></script>

    <script src='//geoip.radio-canada.ca/geoip.js'></script>
    <script src='http://s.radio-canada.ca/_js/modules/modules.pub.manuel.js'></script>
    <script src='http://s.radio-canada.ca/_js/modules/pub.fixnav.js'></script>
    <script type="text/javascript" src="http://s.radio-canada.ca/mp/viafoura.js"></script>
    <script type="text/javascript" src="http://s.radio-canada.ca/mp/modules.socialbar.executer.6.0.js"></script>

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css">

    <script type="text/javascript" src="https://services.radio-canada.ca/media/player/client/<?= $clientId ?>"></script>

    <style>
        .absolute-decoration{
            position: absolute;
            z-index: 100;
            width: 100px;
            height: 100px;
            background: #60FF22;
            top:10%;
            left:10%;
        }

        .v10core-rcplayer-mod-fixed{
            max-width: 25vw;
            max-height: 25vh;
            top:15px;
            left:15px;
        }
    </style>
</head>

<body class="screen-medium">
<script src='http://s.radio-canada.ca/omniture/omni_stats_base.js?version=2014110501'></script>
<div id="hostPubBanner"></div>
<div id="hostNavSticky"></div>
<div id="hostNavigation"></div>
<div id="hostNavigationZone"></div>
<div id="src-container">
    <div id="src-content" class="clearfix" tabindex="0">

        <h2>Default no UI</h2>
        <div data-player-default-no-ui></div>
        <h2>Default</h2>
        <div data-player-default2></div>
        <h2>FancyBox</h2>
        <div data-player-fancybox></div>
        <h2>Default with Attr</h2>
        <div data-v10core-rcplayer='{"core": {"debugMode": true}, "media": {"idMedia":"<?= $medianetId ?>"}, "events": {"PLAY":"onPlayVideo"}}'></div>
        <h2>Default with Attr + Ajax</h2>
        <div data-custom-attr='{"core": {"debugMode": true}, "media": {"idMedia":"<?= $medianetId ?>"}}'></div>
        <h2>Absolute mod</h2>
        <div style="position: relative;">
            <div data-player-absolute></div>
            <div class="absolute-decoration"></div>
        </div>
        <h2>Fixed mod</h2>
        <div data-player-fixed></div>










        <script>if (typeof define != "undefined") define.amd = false;</script>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

        <script src="https://use.fontawesome.com/2deb97109c.js"></script>

        <script type="text/javascript" src="dist/RCPlayer.min.js"></script>

        <script>

            function onPlayVideo(rcPlayer){
                console.log("playVideo Attr !", rcPlayer);
            }

            jQuery(document).ready(function ($) {

                //Example default no ui
                var rcDefaultNoUI = new V10Core.RCPlayer({
                    core: {
                        container: "[data-player-default-no-ui]",
                        name: "Player no ui",
                        debugMode: true
                    },
                    media:{
                      idMedia:"<?= $medianetId ?>"
                    },
                    events: {
                        PLAY: function (rcPlayer) {
                            console.log("play!", rcPlayer);
                        }
                    }
                });

                //Example default
                var rcDefault2 = new V10Core.RCPlayer({
                    core: {
                        container: "[data-player-default2]",
                        name: "Player default",
                        debugMode: true
                    },
                    media:{
                        idMedia:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoPlay:true
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    events: {
                        PLAY: function (rcPlayer) {
                            console.log("play!", rcPlayer);
                        }
                    }
                });

                //Example fancybox
                var rcFancybox = new V10Core.RCPlayer({
                    core: {
                        container: "[data-player-fancybox]",
                        name: "Player FancyBox",
                        debugMode: true
                    },
                    media:{
                        idMedia:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoPlay:true
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    events: {
                        PLAY: function (rcPlayer) {
                            console.log("play!", rcPlayer);
                        }
                    },
                    mod:V10Core.RCPlayer.mods.FANCY_BOX
                });

                //Example fancybox auto open
                var rcFancybox = new V10Core.RCPlayer({
                    core: {
                        name: "Player FancyBox auto open",
                        debugMode: true
                    },
                    media:{
                        idMedia:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoPlay:true
                    },
                    mod:V10Core.RCPlayer.mods.FANCY_BOX
                });

                //Custom
                V10Core.RCPlayer.initPlayerByAttr("data-custom-attr");

                //Example Absolute
                var rcFancybox = new V10Core.RCPlayer({
                    core: {
                        container: "[data-player-absolute]",
                        name: "Player Absolute",
                        debugMode: true
                    },
                    media:{
                        idMedia:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoPlay:true
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.RCPlayer.mods.ABSOLUTE
                });

                //Example Fixed
                var rcFancybox = new V10Core.RCPlayer({
                    core: {
                        container: "[data-player-fixed]",
                        name: "Player Fixed",
                        debugMode: true
                    },
                    media:{
                        idMedia:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoPlay:true
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.RCPlayer.mods.FIXED
                });

            });
        </script>

    </div>
</div>

<div id="hostFooter"></div>

<script>
    /*
     Sections disponibles
     --------------
     abitibi-temiscamingue
     acadie
     alberta
     colombie-britannique-et-yukon
     est-du-quebec
     estrie
     grandmontreal
     manitoba
     mauricie
     ontario
     ottawa-gatineau
     quebec
     saguenay-lac-saint-jean
     saskatchewan

     info
     premiere
     tele
     */

    var section = 'tele';

    function loadAdvertising() {
        bunkerRequire(['underscore', 'services/componentsService', 'externals/components/adsmanager/main'], function (_, cpSvc, adsmanager) {
            adsmanager.init();
            adsmanager.processInit();
        });
    }

    bunkerRequire(['jquery', 'services/componentsService', 'underscore'], function (jq, cpSvc, _) {
        var navAsync = cpSvc.load('dynamicmenu', {hideleaderboard: true}, document.getElementById('hostNavigation'));
        var footerAsync = cpSvc.load('footer', {}, document.getElementById('hostFooter'));
        var stickyNavAsync = cpSvc.load('StickyNav', {
            itemSelector: '#hostNavigation',
            url: section
        }, document.getElementById('hostNavSticky'));

        var bannerAsync = cpSvc.load('banner', {}, document.getElementById('hostPubBanner'));

        if (section === 'tele') {
            var navZoneTeleASync = cpSvc.load('menuZone', {}, document.getElementById('hostNavigationZone'));
        }


        /* Exemple d'injection d'un bigbox */
        var bigBoxAsync = cpSvc.load('bigbox', {external: true}, document.getElementById('hostPubBigBox'));

        /* Vous pouvez utiliser les paramètres facultatifs suivants:

         showOnlyWhenView (Valeur par défaut: false): Charger la publicité seulement lorsqu'elle apparaît dans le visuel de l'usager.
         hideOnMobile (Valeur par défaut: false): Ne pas charger la publicité sur un téléphone.
         hideOnTabletMd (Valeur par défaut: false): Ne pas charger la publicité sur une tabelette en format portail.
         hideOnTabletLg (Valeur par défaut: false): Ne pas charger la publicité sur une tablette en format paysage.
         hideOnDesktop (Valeur par défaut: false): Ne pas charger la publicité sur un ordinateur.
         hidePubLabel (Valeur par défaut: false): Ne pas afficher la mention publicité. (Elle est obligatoire, il faut que vous la gérer manuellement si vous retirer celà. C'est une règle d'accèssiblité.)
         */

        jq.when(navAsync, footerAsync, stickyNavAsync, bannerAsync, bigBoxAsync).done(function (nav, footer, sticky, banner, bigbox) {
            // Fournir un délai pour être certain de ne pas tenter de charger les publicités avant que les fonctions init de chaque composant ait été appelé.
            _.delay(loadAdvertising, 500);
        });
    });
</script>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>

</html>